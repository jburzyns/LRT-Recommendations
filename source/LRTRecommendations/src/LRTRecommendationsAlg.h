#ifndef LRTUNCERTAINTIES_LRTUNCERTAINTIESALG_H
#define LRTUNCERTAINTIES_LRTUNCERTAINTIESALG_H

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

// ROOT Includes
#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"

// xAOD Includes
#include "xAODTracking/Vertex.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"

// standard includes
#include <string>
#include <map>

   // util
namespace DVUtil {
  enum class VertexRegion {
      inclusive = 0,

      insideBeamPipe,

      insidePixelBarrel0,
      aroundPixelBarrel0,

      outsidePixelBarrel0_and_insidePixelBarrel1,
      aroundPixelBarrel1,

      outsidePixelBarrel1_and_insidePixelBarrel2,
      aroundPixelBarrel2,

      outsidePixelBarrel2_and_insidePixelBarrel3,
      aroundPixelBarrel3,

      outsidePixelBarrel3_and_insideSctBarrel0,
      aroundSctBarrel0,

      outsideSctBarrel0_and_insideSctBarrel1,
      aroundSctBarrel1,

      elsewhere,

      /// Not a valid value; this is the number of members in this enum
      nRegions,
      // helpers for iterating over the enum
      begin = 0,
      end = nRegions
  };

  std::string enum_to_string(VertexRegion r){
      switch(r) {
        case VertexRegion::inclusive:
          return "inclusive";
        case VertexRegion::insideBeamPipe:
          return "insideBeamPipe";
        case VertexRegion::insidePixelBarrel0:
          return "insidePixelBarrel0";
        case VertexRegion::aroundPixelBarrel0:
          return "aroundPixelBarrel0";
        case VertexRegion::outsidePixelBarrel0_and_insidePixelBarrel1:
          return "outsidePixelBarrel0_and_insidePixelBarrel1";
        case VertexRegion::aroundPixelBarrel1:
          return "aroundPixelBarrel1";
        case VertexRegion::outsidePixelBarrel1_and_insidePixelBarrel2:
          return "outsidePixelBarrel1_and_insidePixelBarrel2";
        case VertexRegion::aroundPixelBarrel2:
          return "aroundPixelBarrel2";
        case VertexRegion::outsidePixelBarrel2_and_insidePixelBarrel3:
          return "outsidePixelBarrel2_and_insidePixelBarrel3";
        case VertexRegion::aroundPixelBarrel3:
          return "aroundPixelBarrel3";
        case VertexRegion::outsidePixelBarrel3_and_insideSctBarrel0:
          return "outsidePixelBarrel3_and_insideSctBarrel0";
        case VertexRegion::aroundSctBarrel0:
          return "aroundSctBarrel0";
        case VertexRegion::outsideSctBarrel0_and_insideSctBarrel1:
          return "outsideSctBarrel0_and_insideSctBarrel1";
        case VertexRegion::aroundSctBarrel1:
          return "aroundSctBarrel1";
        case VertexRegion::elsewhere:
          return "elsewhere";
        default:
          return "";
    }
  }

  //____________________________________________________________________________________________________
  enum VertexRegion vertexRegion( const xAOD::Vertex* dv ) {
    // Mutually exclusive vertex position pattern
    VertexRegion area;

    const double rad = dv->position().perp();
    const double absz = fabs( dv->z() );

    if( rad < 23.50 ) {
      area = VertexRegion::insideBeamPipe;

    } else if( rad < 31.0 && absz < 331.5 ) {
      area = VertexRegion::insidePixelBarrel0;

    } else if( rad < 38.4 && absz < 331.5 ) {
      area = VertexRegion::aroundPixelBarrel0;

    } else if( rad < 47.7 && absz < 400.5 ) {
      area = VertexRegion::outsidePixelBarrel0_and_insidePixelBarrel1;

    } else if( rad < 54.4 && absz < 400.5 ) {
      area = VertexRegion::aroundPixelBarrel1;

    } else if( rad < 85.5 && absz < 400.5 ) {
      area = VertexRegion::outsidePixelBarrel1_and_insidePixelBarrel2;

    } else if( rad < 92.2 && absz < 400.5 ) {
      area = VertexRegion::aroundPixelBarrel2;

    } else if( rad < 119.3 && absz < 400.5 ) {
      area = VertexRegion::outsidePixelBarrel2_and_insidePixelBarrel3;

    } else if( rad < 126.1 && absz < 400.5 ) {
      area = VertexRegion::aroundPixelBarrel3;

    } else if( rad < 290 && absz < 749.0 ) {
      area = VertexRegion::outsidePixelBarrel3_and_insideSctBarrel0;

    } else if( rad < 315 && absz < 749.0 ) {
      area = VertexRegion::aroundSctBarrel0;

    } else if( rad < 360 && absz < 749.0 ) {
      area = VertexRegion::outsideSctBarrel0_and_insideSctBarrel1;

    } else if( rad < 390 && absz < 749.0 ) {
      area = VertexRegion::aroundSctBarrel1;

    } else {
      area = VertexRegion::elsewhere;
    }
    return area;
  }
}

class LRTRecommendationsAlg: public ::AthAnalysisAlgorithm { 
 public: 
  LRTRecommendationsAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~LRTRecommendationsAlg(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 

   TLorentzVector getVertexFourVec(const xAOD::Vertex* secVtx);
   StatusCode fillHists(std::string region, const xAOD::Vertex* secVtx);

   // properties
   std::string m_secondaryVertexContainer_SGKey = "";

   // member vars
   int m_eventCount = 0;

   // Ks histograms
   std::map<std::string, TH1D*>  m_Ks_Pt;
   std::map<std::string, TH1D*>  m_Ks_Eta;
   std::map<std::string, TH1D*>  m_Ks_Phi;
   std::map<std::string, TH1D*>  m_Ks_M;
   std::map<std::string, TH1D*>  m_Ks_X;
   std::map<std::string, TH1D*>  m_Ks_Y;
   std::map<std::string, TH1D*>  m_Ks_Z;
   std::map<std::string, TH1D*>  m_Ks_R;
   std::map<std::string, TH2D*>  m_Ks_R_Z;
   std::map<std::string, TH2D*>  m_Ks_R_Pt;
   std::map<std::string, TH2D*>  m_Ks_Eta_Phi;
   std::map<std::string, TH1D*>  m_trk_qOverP;
   std::map<std::string, TH1D*>  m_trk_theta; 
   std::map<std::string, TH1D*>  m_trk_E; 
   std::map<std::string, TH1D*>  m_trk_M; 
   std::map<std::string, TH1D*>  m_trk_Pt;  
   std::map<std::string, TH1D*>  m_trk_Px; 
   std::map<std::string, TH1D*>  m_trk_Py; 
   std::map<std::string, TH1D*>  m_trk_Pz;  
   std::map<std::string, TH1D*>  m_trk_Eta; 
   std::map<std::string, TH1D*>  m_trk_Phi;
   std::map<std::string, TH1D*>  m_trk_D0; 
   std::map<std::string, TH1D*>  m_trk_Z0;
   std::map<std::string, TH1D*>  m_trk_errD0; 
   std::map<std::string, TH1D*>  m_trk_errZ0; 
   std::map<std::string, TH1D*>  m_trk_Chi2;
   std::map<std::string, TH1D*>  m_trk_nDoF; 
   std::map<std::string, TH1D*>  m_trk_charge;
   std::map<std::string, TH1D*>  m_Ks_ntrk_lrt;

   // Ks ntuple
   TTree* m_KsTree = nullptr;

}; 

#endif //> !LRTUNCERTAINTIES_LRTUNCERTAINTIESALG_H
