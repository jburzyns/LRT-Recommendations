// LRTRecommendations includes
#include "LRTRecommendationsAlg.h"

#include "AthenaKernel/Units.h"

#include "xAODEventInfo/EventInfo.h"

// ROOT includes
#include "TMath.h"

using Athena::Units::GeV;


LRTRecommendationsAlg::LRTRecommendationsAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  declareProperty( "SecondaryVertexContainer_SGKey", m_secondaryVertexContainer_SGKey = "", "Secondary Vertex Container StoreGate key" ); 

}


LRTRecommendationsAlg::~LRTRecommendationsAlg() {}


StatusCode LRTRecommendationsAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  // loop over detector regions and add hists to map
  for(DVUtil::VertexRegion r = DVUtil::VertexRegion::begin; r < DVUtil::VertexRegion::end; r = static_cast<DVUtil::VertexRegion>((size_t)r + 1)) {

    std::string region = DVUtil::enum_to_string(r);

    // Create histograms and TTree and register them to the histsvc
    m_Ks_M[region]        = new TH1D(("Ks_M_" + region).c_str()  ,"Ks_M"  ,200,400,600   );   // units: GeV
    m_Ks_Pt[region]       = new TH1D(("Ks_Pt_" + region).c_str() ,"Ks_Pt" ,500,0,100 );   // units: GeV
    m_Ks_Eta[region]      = new TH1D(("Ks_Eta_" + region).c_str(),"Ks_Eta",500,-5,5  );
    m_Ks_Phi[region]      = new TH1D(("Ks_Phi_" + region).c_str(),"Ks_Phi" ,500,-TMath::Pi(),TMath::Pi());
    m_Ks_X[region]        = new TH1D(("Ks_X_" + region).c_str()  ,"Ks_X"  ,500,-500,500);   // units: mm
    m_Ks_Y[region]        = new TH1D(("Ks_Y_" + region).c_str()  ,"Ks_Y"  ,500,-500,500);   // units: mm
    m_Ks_Z[region]        = new TH1D(("Ks_Z_" + region).c_str()  ,"Ks_Z"  ,500,-500,500);   // units: mm
    m_Ks_R[region]        = new TH1D(("Ks_R_" + region).c_str()  ,"Ks_R"  ,500,0,   500);   // units: mm

    m_Ks_R_Z[region]      = new TH2D(("Ks_R_Z" + region).c_str()     ,"Ks_R_Z"      , 500, 0, 500, 500, -500, 500);
    m_Ks_R_Pt[region]     = new TH2D(("Ks_R_Pt_" + region).c_str()    ,"Ks_R_Pt"     , 500, 0, 500, 500, 0, 100);
    m_Ks_Eta_Phi[region]  = new TH2D(("Ks_Eta_Phi_" + region).c_str() ,"Ks_Eta_Phi"  , 500, -5, 5, 500,-TMath::Pi(),TMath::Pi()); 

    m_Ks_ntrk_lrt[region]  = new TH1D(("Ks_ntrk_lrt_" + region).c_str() ,"Ks_ntrk_lrt"  , 3, -0.5, 2.5); 

    // tracks
    m_trk_qOverP[region]     = new TH1D(("trk_qOverP_" + region).c_str(),"trk_qOverP",100,0,.01);  
    m_trk_theta[region]      = new TH1D(("trk_theta_" + region).c_str(), "trk_theta",64,0,3.2);  
    m_trk_E[region]          = new TH1D(("trk_E_" + region).c_str(),"trk_E",100,0,100); 
    m_trk_M[region]          = new TH1D(("trk_M_" + region).c_str(),"trk_M",100,0,10);  
    m_trk_Pt[region]         = new TH1D(("trk_Pt_" + region).c_str(),"trk_Pt",100,0,100);  
    m_trk_Px[region]         = new TH1D(("trk_Px_" + region).c_str(),"trk_Px",100,0,100);  
    m_trk_Py[region]         = new TH1D(("trk_Py_" + region).c_str(),"trk_Py",100,0,100);  
    m_trk_Pz[region]         = new TH1D(("trk_Pz_" + region).c_str(),"trk_Pz",100,0,100);  
    m_trk_Eta[region]        = new TH1D(("trk_Eta_" + region).c_str(),"trk_Eta",100,-5,5);  
    m_trk_Phi[region]        = new TH1D(("trk_Phi_" + region).c_str(),"trk_Phi",63,-3.2,3.2); 
    m_trk_D0[region]         = new TH1D(("trk_D0_" + region).c_str(),"trk_D0 ",300,-300,300); 
    m_trk_Z0[region]         = new TH1D(("trk_Z0_" + region).c_str(),"trk_Z0 ",500,-500,500); 
    m_trk_errD0[region]      = new TH1D(("trk_errD0_" + region).c_str(),"trk_errD0",300,0,30);  
    m_trk_errZ0[region]      = new TH1D(("trk_errZ0_" + region).c_str(),"trk_errZ0",500,0,50);  
    m_trk_Chi2[region]       = new TH1D(("trk_Chi2_" + region).c_str(),"trk_Chi2",100,0,10); 
    m_trk_nDoF[region]       = new TH1D(("trk_nDoF_" + region).c_str(),"trk_nDoF ",100,0,100);  
    m_trk_charge[region]     = new TH1D(("trk_charge_" + region).c_str(),"trk_charge",3,-1.5,1.5); 

    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/Ks_M",   m_Ks_M[region])   );   
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/Ks_Pt",  m_Ks_Pt[region])  ); 
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/Ks_Eta", m_Ks_Eta[region]) ); 
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/Ks_Phi", m_Ks_Phi[region]) ); 
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/Ks_R",   m_Ks_R[region]) ); 
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/Ks_Z",   m_Ks_Z[region]) ); 
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/Ks_R_Z", m_Ks_R_Z[region]) ); 
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/Ks_R_Pt", m_Ks_R_Pt[region]) ); 
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/Ks_Eta_Phi", m_Ks_Eta_Phi[region]) ); 
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/Ks_ntrk_lrt", m_Ks_ntrk_lrt[region]) ); 
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_qOverP", m_trk_qOverP[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_theta", m_trk_theta[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_E", m_trk_E[region]));    
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_M", m_trk_M[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_Pt", m_trk_Pt[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_Px", m_trk_Px[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_Py", m_trk_Py[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_Pz", m_trk_Pz[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_Eta", m_trk_Eta[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_Phi", m_trk_Phi[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_D0", m_trk_D0[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_Z0", m_trk_Z0[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_errZ0", m_trk_errZ0[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_errD0", m_trk_errD0[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_Chi2", m_trk_Chi2[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_nDoF", m_trk_nDoF[region]));
    ATH_CHECK( histSvc()->regHist("/KSSTREAM/" + region + "/trk_charge", m_trk_charge[region]));
  }


  m_KsTree = new TTree("KsTree","KsTree");
  CHECK( histSvc()->regTree("/KSSTREAM/KsTree", m_KsTree) ); //registers tree to output stream inside a sub-directory

  return StatusCode::SUCCESS;
}

StatusCode LRTRecommendationsAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  return StatusCode::SUCCESS;
}

// TODO: write function to determine vertex region 
StatusCode LRTRecommendationsAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ATH_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // print out run and event number from retrieved object
  if(m_eventCount % 1000 == 0) {
    ATH_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
  }
  m_eventCount++;

  // retrieve the SecondaryVertexContainer object from the store
  const xAOD::VertexContainer *secondaryVertexContainer = nullptr;
  ATH_CHECK (evtStore()->retrieve (secondaryVertexContainer, m_secondaryVertexContainer_SGKey));

  // begin loop over vertices
  for(const xAOD::Vertex* secVtx: *secondaryVertexContainer) {

    std::string region = DVUtil::enum_to_string(DVUtil::vertexRegion(secVtx));
    ATH_CHECK(fillHists(region, secVtx));
    ATH_CHECK(fillHists("inclusive", secVtx));

  } // end loop over vertices

  return StatusCode::SUCCESS;
}

TLorentzVector LRTRecommendationsAlg::getVertexFourVec(const xAOD::Vertex* secVtx) {

    TLorentzVector vtx_p4(0,0,0,0);

    if(m_secondaryVertexContainer_SGKey.find("VrtSecInclusive") != std::string::npos) {
      xAOD::Vertex::ConstAccessor<xAOD::Vertex::TrackParticleLinks_t> trkAcc("trackParticleLinks");
      const xAOD::Vertex::TrackParticleLinks_t & trkParts = trkAcc( *secVtx );
      // set up track aux accessors
      xAOD::TrackParticle::ConstAccessor< float > ptAcc( "pt_wrtSV" );
      xAOD::TrackParticle::ConstAccessor< float > etaAcc( "eta_wrtSV" );
      xAOD::TrackParticle::ConstAccessor< float > phiAcc( "phi_wrtSV" );

      // get the two tracks in the vertex
      const xAOD::TrackParticle & trk1 = **trkParts[0];
      const xAOD::TrackParticle & trk2 = **trkParts[1];

      TLorentzVector trk1_P4, trk2_P4;
      trk1_P4.SetPtEtaPhiM(ptAcc(trk1), etaAcc(trk1), phiAcc(trk1), trk1.p4().M());
      trk2_P4.SetPtEtaPhiM(ptAcc(trk2), etaAcc(trk2), phiAcc(trk2), trk2.p4().M());

      vtx_p4 = trk1_P4 + trk2_P4;

    }
    else if(m_secondaryVertexContainer_SGKey.find("V0Candidates") != std::string::npos) {

      xAOD::Vertex::ConstAccessor<float> px("px");
      xAOD::Vertex::ConstAccessor<float> py("py");
      xAOD::Vertex::ConstAccessor<float> pz("pz");
      xAOD::Vertex::ConstAccessor<float> m("Kshort_mass");

      vtx_p4.SetXYZM(px(*secVtx), py(*secVtx), pz(*secVtx), m(*secVtx));

    }

    return vtx_p4;

}

StatusCode LRTRecommendationsAlg::fillHists(std::string region, const xAOD::Vertex* secVtx) { 

    // set of accessors for tracks and weights
    xAOD::Vertex::ConstAccessor<xAOD::Vertex::TrackParticleLinks_t> trkAcc("trackParticleLinks");
    xAOD::Vertex::ConstAccessor<float> Chi2("chiSquared");
    xAOD::Vertex::ConstAccessor<float> nDoF("numberDoF");

    const xAOD::Vertex::TrackParticleLinks_t & trkParts = trkAcc( *secVtx );

    //if don't have track particles
    if (!trkAcc.isAvailable(*secVtx) ) {
      ATH_MSG_WARNING("trackParticles not available, vertex is missing info");
      return StatusCode::SUCCESS;
    }

    size_t vtx_ntrk = trkParts.size();

    if(vtx_ntrk != 2) return StatusCode::SUCCESS;

    // get the two tracks in the vertex
    if(!trkParts[0].isValid() or !trkParts[1].isValid()) {
      ATH_MSG_WARNING ("No valid trackParticleLinks!");
    }
    /*
    const xAOD::TrackParticle & trk1 = **trkParts[0];
    const xAOD::TrackParticle & trk2 = **trkParts[1];

    int charge = trk1.charge() + trk2.charge();
    // only consider neutral DVs
    if(charge != 0) return StatusCode::SUCCESS;
    */ 

    TLorentzVector vtx_sumP4 = getVertexFourVec(secVtx);

    if(vtx_sumP4.M() < 472 or vtx_sumP4.M() > 522) return StatusCode::SUCCESS;
  
    xAOD::TrackParticle::ConstAccessor< std::vector< float > > accCovMatrixDiag( "definingParametersCovMatrixDiag" );

    int ntrk_lrt = 0;

    // loop over tracks and fill track histograms
    for(size_t i = 0; i < vtx_ntrk; i++){
      if(!trkParts[i].isValid()) {
        ATH_MSG_WARNING("Invalid track particle link!");
        continue;
      }
      const xAOD::TrackParticle & trk = **trkParts[i];

      const std::bitset<xAOD::NumberOfTrackRecoInfo> patternReco = trk.patternRecoInfo();
      if(patternReco.test(49)) ntrk_lrt++;

      double trk_d0 = trk.definingParameters()[0];
      double trk_z0 = trk.definingParameters()[1];

      m_trk_errD0[region]  ->Fill(trk.definingParametersCovMatrix()(0,0));
      m_trk_errZ0[region]  ->Fill(trk.definingParametersCovMatrix()(1,1));
      m_trk_theta[region]  ->Fill(trk.definingParameters()[3]);
      m_trk_qOverP[region] ->Fill(trk.definingParameters()[4]);
      m_trk_E[region]      ->Fill(trk.e()/GeV);
      m_trk_M[region]      ->Fill(trk.m()/GeV);
      m_trk_Pt[region]     ->Fill(trk.pt()/GeV);
      m_trk_Px[region]     ->Fill(trk.p4().Px()/GeV);
      m_trk_Py[region]     ->Fill(trk.p4().Py()/GeV);
      m_trk_Pz[region]     ->Fill(trk.p4().Pz()/GeV);
      m_trk_Eta[region]    ->Fill(trk.eta());
      m_trk_Phi[region]    ->Fill(trk.phi0());
      m_trk_D0[region]     ->Fill(trk_d0);
      m_trk_Z0[region]     ->Fill(trk_z0);
      m_trk_charge[region] -> Fill(trk.charge());
    } // end loop over tracks

    m_Ks_ntrk_lrt[region] ->Fill(ntrk_lrt);

    float vtx_x = secVtx->x();
    float vtx_y = secVtx->y();
    float vtx_z = secVtx->z();

    TVector3 vtxPos(vtx_x, vtx_y, vtx_z);
    float vtx_r = vtxPos.Perp();
    
    m_Ks_X[region]   ->Fill(vtx_x);
    m_Ks_Y[region]   ->Fill(vtx_y);
    m_Ks_Z[region]   ->Fill(vtx_z);
    m_Ks_R[region]   ->Fill(vtx_r);
    m_Ks_R_Z[region] ->Fill(vtx_r, vtx_z);

    float vtx_pt  = vtx_sumP4.Pt();
    float vtx_eta = vtx_sumP4.Eta();
    float vtx_phi = vtx_sumP4.Phi();
    float vtx_m   = vtx_sumP4.M();

    m_Ks_Pt[region]  ->Fill(vtx_pt/GeV);
    m_Ks_Eta[region] ->Fill(vtx_eta);
    m_Ks_Phi[region] ->Fill(vtx_phi);
    m_Ks_M[region]   ->Fill(vtx_m);

    m_Ks_R_Pt[region]    ->Fill(vtx_r, vtx_pt/GeV);
    m_Ks_Eta_Phi[region] ->Fill(vtx_eta, vtx_phi);

  return StatusCode::SUCCESS;
}

StatusCode LRTRecommendationsAlg::beginInputFile() { 

  return StatusCode::SUCCESS;
}


