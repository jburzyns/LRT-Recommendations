import sys
import ROOT

if __name__ == '__main__':

  inputFile = ROOT.TFile(sys.argv[1])
  assert(inputFile)

  inputHist = inputFile.Get("inclusive/Ks_M_inclusive")
  assert(inputHist)

  integral = inputHist.Integral()
  assert(integral == 4974.0)
