#!/usr/bin/env python

"""KsFit.py

This module performs fits on the Ks mass peak to estimate the total Ks yield.
It takes as input the histograms output from the LRTUncertainties package.

Example:
    To run the script, you must specify a path to an input root file
    containing the Ks mass histograms in each region:

        $ python KsFit.py /path/to/my/root/file.root

Todo:
    * Make fit hypothesis configurable

"""

import argparse
import sys
import numpy as np
import ROOT

class multiGausBkg:
    def __call__( self, t, par ):
        a1 = par[0] # amp1
        mu = par[1] # mean
        s1 = par[2] # sigma1
        a2 = par[3] # amp2
        s2 = par[4] # sigma2

        m  = par[5] # linear slope
        b  = par[6] # constant

        x = t[0]
        return a1*np.exp(-0.5*((x-mu)/s1)**2) + a2*np.exp(-0.5*((x-mu)/s2)**2) + m*x + b

class multiGaus:
    def __call__( self, t, par ):
        a1 = par[0] # amp1
        mu = par[1] # mean
        s1 = par[2] # sigma1
        a2 = par[3] # amp2
        s2 = par[4] # sigma2

        x = t[0]
        return a1*np.exp(-0.5*((x-mu)/s1)**2) + a2*np.exp(-0.5*((x-mu)/s2)**2)

def fit(hist):

    parN = 7
    func = ROOT.TF1("func", multiGausBkg(), 400, 600, parN)
    func.SetParameters(hist.Integral()/20, 500, 50, hist.Integral()/20, 50, 0, hist.Integral()/100)

    hist.Fit(func)

    return func.GetParameters()

def get_args():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        'files',
        help='root file(s) to fit',
        nargs='*'
    )

    args = parser.parse_args()
    if not args.files:
        parser.print_usage()
        sys.exit('ERROR: need to provide a path to valid root file')
    return args


def main():
    args = get_args()

    files = args.files

    c2 = ROOT.TCanvas( 'c2', 'fit', 800, 600 )

    f = ROOT.TFile(files[0])
    h = f.Get("inclusive/Ks_M_inclusive")

    fit_results = fit(h)

    a1 = fit_results[0] # amp1
    mu = fit_results[1] # mean
    s1 = fit_results[2] # sigma1
    a2 = fit_results[3] # amp2
    s2 = fit_results[4] # sigma2
    m  = fit_results[5] # linear slope
    b  = fit_results[6] # constant

    result = ROOT.TF1("result", multiGausBkg(), 400, 600, 7)
    result.SetParameters(a1, mu, s1, a2, s2, m, b)

    g1 = ROOT.TF1("g1","gaus",400,600,3)
    g1.SetParameters(a1,mu,s1)
    g1.SetLineColor(ROOT.kBlue)

    g2 = ROOT.TF1("g2","gaus",400,600,3)
    g2.SetParameters(a2,mu,s2)
    g2.SetLineColor(ROOT.kBlack)

    l1 = ROOT.TF1("l1","pol1",400,600,2)
    l1.SetParameters(b,m)
    l1.SetLineColor(ROOT.kGreen)

    h.Draw("hist")
    result.Draw("same C")
    g1.Draw("same C")
    g2.Draw("same C")
    l1.Draw("same C")

    c2.Print("test.pdf")


if __name__ == "__main__":
  main()
