from glob import glob
jps.AthenaCommonFlags.FilesInput = glob("/eos/atlas/atlascerngroupdisk/perf-idtracking/LRTRun3Developments/Recommendations/TestFiles/user.jburzyns.900149.PG_single_nu_Pt50.merge.DAOD_IDTR2.e8307_s3481_r13050_r12253_r13050_r12636_p0006_EXT0/user.jburzyns.28090419.EXT0._000030.DAOD_IDTR2.test.pool.root") #must happen before the import of the read file mode

jps.AthenaCommonFlags.AccessMode = "ClassAccess" 

# set up the output stream
jps.AthenaCommonFlags.HistOutputs = ["KSSTREAM:KsHist.root"]  

# add alg to the sequence
athAlgSeq += CfgMgr.LRTRecommendationsAlg(SecondaryVertexContainer_SGKey = "IDTR2RecoV0Candidates")  
#athAlgSeq += CfgMgr.LRTRecommendationsAlg(SecondaryVertexContainer_SGKey = "VrtSecInclusive_SecondaryVertices")  

include("AthAnalysisBaseComps/SuppressLogging.py")              
