# LRT-Uncertainties

This is a package for studies related to deriving recommendations for LRT
tracks in R22.

## Setting up

Start by setting things up and cloning the repository
```
setupATLAS
lsetup git
git clone --recursive https://:@gitlab.cern.ch:8443/jburzyns/LRT-Recommendations.git 
```
Migrate into the repository and set up your directory structure
```
cd LRT-Recommendations
mkdir build run
```
To configure the build we will use `acmSetup` in the `build` directory
```
cd build
acmSetup AthAnalysis,22.2.56
```
On future logins, you will just need to run
```
cd build
acmSetup
```
Next, compile the code using 
```
acm compile
```

## Running

Navigate the `run` directory, and run over the default test file on eos
```
cd $TestArea/../run
athena LRTRecommendations/LRTRecommendationsAlgJobOptions.py
```
